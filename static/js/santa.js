
window.onload = function() {
	var secretbtn = document.getElementById("btn-getsecret");
	secretbtn.onclick = function() {
		$.ajax({
		  url: "/secretsanta",
		  data: "get",
		  success: santa_success,
		  dataType: "json"
		});
	}
	
	$("#debug")[0].innerHTML = "<p>screen width: " + $(window)[0].screen.width + " " + $(window)[0].screen.availWidth + "</p>"
	var deb = document.getElementById("xmastree1");
	deb.onclick = function() {
		$("#debug")[0].className = "debug-show";
	}
}

function santa_success(data, textStatus, jqXHR) {
	//$("#result")[0].innerHTML = "<p>I got stuff back: name: " + data.Name + ", status: " + data.Status + "</p>";
	if (data.Status == 404) {
		$("#btn-getsecret .getsecret")[0].innerHTML = "All The Names Are Taken";
	} else {
		$("#btn-getsecret")[0].className = "btn-getsecret-hide";
		$(".secretname")[0].innerHTML = data.Name +"!";
		$("#debug")[0].innerHTML = data.Cid;
		$(".secretname")[0].className = "secretname-show";
		setTimeout(function(){$(".yougot")[0].className = "yougot-show"}, 2500)
		setTimeout(snowinit, 6000);	
	}
}