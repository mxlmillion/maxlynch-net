package admin

import(
	"net/http"
	"io"
	"fmt"
	"strings"
	"encoding/hex"
	"encoding/json"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
	//"maxlynch.net/mongo_layer"
)

type DBHandler interface {
	AddPage(Page) ([]byte, error)
	FindPage([]byte) (Page, error)
	FindPageByName(string) (Page, error)
	FindAllAvailablePages() ([]Page, error)
}

type Page struct {
	ID bson.ObjectId `bson:"_id"`
	Name string
	Title string
	CreateDate int64
	UpdateDate int64
	HTMLBody string
	HTMLComponents map[string]bool
	Capabilities []string

}

type AdminHTTPReqHandler struct {
	dbhandler DBHandler
}



func ServeAPI(endpoint string, dbHandler DBHandler ) error {

	a := newAdminHTTPReqHandler(dbHandler)
	r := mux.NewRouter()
	adminrouter := r.PathPrefix("/admin").Subrouter()
	//adminrouter.Methods("GET").Path("").HandlerFunc(a.ReqHandler)
	adminrouter.Methods("GET").Path("/pages/{SearchCriteria}/{search}").HandlerFunc(a.PagesReqHandler)
	return http.ListenAndServe(endpoint, r)
}

func newAdminHTTPReqHandler(dbh DBHandler) *AdminHTTPReqHandler {
	return &AdminHTTPReqHandler{
		dbhandler: dbh,
	}
}

func (h *AdminHTTPReqHandler) ReqHandler(res http.ResponseWriter, req *http.Request) {

	io.WriteString(res, "<p>admin area</p>")
}

func (h *AdminHTTPReqHandler) PagesReqHandler(res http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	criteria, ok := vars["SearchCriteria"]
	if ! ok {
		res.WriteHeader(400)
		fmt.Fprint(res, `[error: no search criteria found.]`)
		return
	}
	searchkey, ok := vars["search"]
	if ! ok {
		res.WriteHeader(400)
		fmt.Fprint(res, `[error: no search id or name found.]`)
		return
	}
	var page Page
	var err error
	switch strings.ToLower(criteria) {
		case "name":
			page, err = h.dbhandler.FindPageByName(searchkey)
		case "id":
			id, err := hex.DecodeString(searchkey)
			if err == nil {
				page, err = h.dbhandler.FindPage(id)
			}
	}
	if err != nil {
		fmt.Fprint(res, "{error %s}", err)
		return
	}

	res.Header().Set("content-type", "application/json;charset=utf8")
	json.NewEncoder(res).Encode(&page)
}