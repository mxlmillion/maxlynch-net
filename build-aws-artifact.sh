#!/bin/bash

APPNAME="ss-maxlynch-net"
APPVERSION="1.0.0"
echo -n "New Application version [current: $APPVERSION]: "
read VERSION 
APPVERSION=$VERSION 

AMDBIN="${APPNAME}-${APPVERSION}.amd64"
AWSBIN="${APPNAME}-${APPVERSION}.bin"
MACOSBIN="${APPNAME}-${APPVERSION}.macos"
AWSKEY=""
AWSSECRET=""
AWSBUCKET="constellation-artifacts/application-artifacts"
AWSBUCKETDIR=""


# echo -n "AWS Secret: "
# read AWSSECRET
# if [ -z $AWSSECRET ]; then
# 	echo "value empty."
# 	exit 1
# fi


# normal linux binary
GOOS=linux GOARCH=amd64 go build -o $AMDBIN maxlynch.net.go
# AWS constellation binary
GOOS=linux GOARCH=amd64 go build -o $AWSBIN maxlynch.net.go
# development binary
GOOS=darwin GOARCH=amd64 go build -o $MACOSBIN maxlynch.net.go

# build AWS artifact
rm -f ss-maxlynch-net.zip
rm -f $APPNAME-$APPVERSION.zip
zip -r $APPNAME-$APPVERSION.zip $AWSBIN templates/ config/ static/
if [ ! $? -eq 0 ]; then
	echo "Failed to build artifact, aborting"
	exit 1
fi

echo "Copying artifact to S3..."
aws s3 cp $APPNAME-$APPVERSION.zip s3://${AWSBUCKET}/$APPNAME/$APPNAME-$APPVERSION.zip
if [ ! $? -eq 0 ]; then
	echo "Something went wrong, aborting"
	exit 1
fi

if [ $? -eq 0 ]; then
	echo "Cleaning up"
	rm $AMDBIN
	rm $AWSBIN
	rm $MACOSBIN
fi
