package main

import (
	"net/http"
	"io"
	"fmt"
	"encoding/json"
	"github.com/gorilla/mux"
	"os"
	"log"
	"flag"
	"strconv"
	"gopkg.in/mgo.v2/bson"
	"html/template"
	"runtime"
	"io/ioutil"
	"math/rand"
	"time"
	"regexp"
	_ "expvar"
	"strings"
	"github.com/google/uuid"
	"github.com/go-redis/redis"
)

/********
	TYPES
 */
type IDBLayer interface {
	AddPage(Page) ([]byte, error)
	FindPage([]byte) (Page, error)
	FindPageByName(string) (Page, error)
	FindAllAvailablePages() ([]Page, error)
}
const (
	DB = "admin"
	USERS = "users"
	PAGES = "pages"
)

const (
	MONGODB DBTYPE = "mongodb"
	DYNAMODB DBTYPE = "dynamodb"
)
type DBTYPE string

type Page struct {
	ID bson.ObjectId `bson:"_id"`
	Name string
	Title string
	CreateDate int64
	UpdateDate int64
	HTMLBody string
	HTMLComponents map[string]bool
	Capabilities []string

}



// New Native types


// Interfaces


// Structs


type PagesAPIReqHandler struct {
	dbhandler IDBLayer
}
type RootReqHandle struct {
	Title string
	GoVersion string
}
type SSantaReqHandle struct {
	Title string
	GoVersion string
}
type AdminReqHandle struct {
	Title string
	GoVersion string
}
type ssJSONresponse struct {
	Name string
	Status int
	Cid string
}

// GLOBAL VARS
var (
	conf ServiceConfig
)


func main() {
	confPath := flag.String("conf", `./config/config.json`, "file path to configuration file")
	flag.Parse()

	// Extract Configuration from file
	conf, _ = ExtractConfiguration(*confPath)

	fmt.Println("Starting with config values:")
	fmt.Printf("%#v\n", conf)
	fmt.Printf("config file: %s\n\n", *confPath)
	//fmt.Println("Connecting to database")
	//dbhandler, _ := NewPersistenceLayer(conf.Databasetype, conf.DBConnection)

	// REST API start
	fmt.Println("Starting server and listening for errors")
	//errchan := ServeAPI(conf.RestfulEndpoint, dbhandler)
	errchan := ServeAPI(conf.RestfulEndpoint)
	select {
		case err := <-errchan:
			log.Fatal("HTTP server error: ", err)
	}
}

func ServeAPI(serverurl string ) chan error {

	// Get our handerl struct, and create the router
	// API isn't ready yet, disable
	//h := newPagesAPIReqHandler(dbHandler)
	//apirouter := r.PathPrefix("/api").Subrouter()

	// Setup handlers for standard HTML site
	fmt.Println("setting up std handlers")
	r := mux.NewRouter()
	rooth := newRootReqHandle()
	r.Path("/").HandlerFunc(rooth.handle)
	r.Path("/index.html").HandlerFunc(rooth.handle)

	fmt.Printf("Setting route handler /secretsanta\n")
	ss := newSSantaReqHandle()
	r.PathPrefix("/secretsanta").HandlerFunc(ss.secretsanta)

	// Setup asset handler
	fmt.Printf("setting up asset handler\n")
	assetrouter := r.PathPrefix("/static").Subrouter()
	assetrouter.Methods("GET").HandlerFunc(rooth.AssetHandler)
	
	// Setup Admin portal handling
	fmt.Printf("setting up admin handler\n")
	adminrouter := r.PathPrefix("/admin").Subrouter()
	adminh := newAdminReqHandle()
	adminrouter.Path("").HandlerFunc(adminh.handleroot)
	
	// TODO: oauth callback
	//rootrouter.Methods("GET").Path("/goog-oauth-callback").HandlerFunc(root.OauthCallback)

	// Async listen to potentially do other stuff
	errchan := make(chan error)
	go func() { errchan <- http.ListenAndServe(serverurl, r) }()
	return errchan
}




/* HTML Site handlers
 */

func newPagesAPIReqHandler(dbh IDBLayer) *PagesAPIReqHandler {
	return &PagesAPIReqHandler{
		dbhandler: dbh,
	}
}

func newRootReqHandle() *RootReqHandle {
	return &RootReqHandle{"Welcome", runtime.Version()}
}

func newSSantaReqHandle() *SSantaReqHandle {
	return &SSantaReqHandle{"Lynch Fam Secret Santa", runtime.Version()}
}

func newAdminReqHandle() *AdminReqHandle {
	return &AdminReqHandle{"Aries Cluster Management", runtime.Version()}
}

func (r *RootReqHandle) handle(res http.ResponseWriter, req *http.Request) {
	fmt.Printf("handling path: %s, rawpath: %s, query:%s\n", req.URL.Path, req.URL.RawQuery, req.URL.RawPath)
	t := template.New("home")
	ts, err := t.ParseFiles("templates/welcome.html.tmpl")
	if err != nil {
		fmt.Printf("error parsing homepage template: %s\n", err)
		os.Exit(1)
	}
	t_welcome := ts.Lookup("welcome.html.tmpl")
	err = t_welcome.ExecuteTemplate(res, "welcome.html.tmpl", r)
	if err != nil {
		fmt.Printf("Error rendering template: %s\n", err)
		res.WriteHeader(500)
		io.WriteString(res, "Error parsing template: " + err.Error())
	}
}

func (r *RootReqHandle) AssetHandler(res http.ResponseWriter, req *http.Request) {
	relpath := req.URL.Path[1:]
    //fmt.Printf("asset handler: serving %v\n", relpath)
    //fmt.Printf("asset handler: header %v\n", res.Header())

    // If this request is for the background, serve a random one
    if relpath == "static/images/backgrounds/bodybg.jpg" {
    	//fmt.Printf("serving background\n")
    	f, err := os.Open("static/images/backgrounds/")
    	if err != nil {
    		res.WriteHeader(500)
    		fmt.Fprintf(res, "error opening background images folder: %s", err)
    	}
    	files, err := f.Readdir(-1)
    	bg := files[getLimitedRandInt(len(files))]
    	//fmt.Printf("Serving background image: %s\n", bg.Name())
    	data, err := ioutil.ReadFile("static/images/backgrounds/" + bg.Name())
    	if err != nil {
	    	res.WriteHeader(500)
	    	fmt.Fprintf(res, "error: %s", err)
		}
		res.Header().Set("Content-Type", "image/jpeg; charset=utf-8")
		res.WriteHeader(http.StatusOK)
		res.Write(data)

	// This is a different asset, server accordingly
    } else {
    	//fmt.Printf("serving asset\n")
    	var cssasset = regexp.MustCompile(`.css$`)
    	var jsasset = regexp.MustCompile(`.js$`)
    	if cssasset.MatchString(req.URL.Path) {
			res.Header().Set("Content-Type", "text/css; charset=utf-8")
    	}
    	if jsasset.MatchString(req.URL.Path) {
    		res.Header().Set("Content-Type", "text/javascript; charset=utf-8")
    	}

    	data, err := ioutil.ReadFile(string(relpath))
    	if err != nil {
	    	res.WriteHeader(500)
	    	fmt.Fprintf(res, "error: %s", err)
		}
		res.WriteHeader(http.StatusOK)
		res.Write(data)
    }
}

func (r *SSantaReqHandle) secretsanta(res http.ResponseWriter, req *http.Request) {
	fmt.Printf("handling path: %s, rawpath: %s, query:%s\n", req.URL.Path, req.URL.RawPath, req.URL.RawQuery)
// Names: Melissa, Nick, Max, Ryan, Glade, Gen, Brittany, Charles, Ryan Hahn, Jackie, Josh, Ursula
	// detect cookie and gen if necessary
	_, err := req.Cookie("clientid")
	if err != nil {
	 	id := uuid.New()
	 	fmt.Printf("setting new clientid cookie: %s\n", id.String())
		c := http.Cookie{Name: "clientid", Value: id.String()}
		http.SetCookie(res, &c)
	}

	// standard request, serve the page and assign a UUID if it doesn't exist
	if len(req.URL.RawQuery) == 0 {
		fmt.Printf("query string empty, generate normal page\n")
		// render template
		t := template.New("secret")
		ts, err := t.ParseFiles("templates/secretsanta.html.tmpl")
		if err != nil {
			fmt.Printf("error parsing homepage template: %s\n", err)
			os.Exit(1)
		}
		t_ss := ts.Lookup("secretsanta.html.tmpl")
		err = t_ss.ExecuteTemplate(res, "secretsanta.html.tmpl", r)
		if err != nil {
			fmt.Printf("Error rendering template: %s\n", err)
			res.WriteHeader(500)
			io.WriteString(res, "Error parsing template: " + err.Error())
		}
	}

	

	// is this a request for a secret santa?
	if req.URL.RawQuery == "get" {
		// User has already visited the site, or requested a secret
		cid, err := req.Cookie("clientid")
		if err != nil {
			fmt.Printf("Error: Cookie clientid doesn't exist.\n", err)
			res.WriteHeader(500)
			io.WriteString(res, "Error: Cookie clientid doesn't exist: " + err.Error())
		}
		fmt.Printf("clientid: %s\n",cid.Value)

		// get connection to redis
		rdb, err := newRedisConnection()
		if err != nil {
			res.WriteHeader(500)
			fmt.Printf("Error connecting to redis: %s\n", err)
			io.WriteString(res, "Error connecting to redis: " + err.Error())
			return 
		}

		// If they already have a secret, resend
		secretname := rdb.Exists(cid.Value + ":secretname")
		if secretname.Val() > 0 {
			secret := rdb.Get(cid.Value + ":secretname").Val()
			fmt.Printf("serving already generated secret to existing client. (name: %s, client: %s)\n", secret, cid.Value)
			resjson := ssJSONresponse{Name: secret, Status: 200, Cid: cid.Value}
			json, _ := json.Marshal(resjson)
			res.Header().Add("Content-Type", "application/json")
			res.Write(json)
		
		// they haven't requested a secret name yet, generate and send
		} else {
			fmt.Printf("generating secret!\n")
			names := rdb.Get("availablenames").Val()
			if len(names) == 0 {
				// this shouldn't happen, but tell the user there are no more names
				resjson := ssJSONresponse{Name: "", Status: 404}
				json, _ := json.Marshal(resjson)
				res.Header().Add("Content-Type", "application/json")
				res.Write(json)
				return
			}

			// get a random name, update redis and return name to user
			namesarr := strings.Split(names, ",")
			secretname := namesarr[ getLimitedRandInt(len(namesarr)) ]

			// rebuilt names array without this name and save
			newnames := []string{}
			for _, v := range namesarr {
				if v == secretname {
					continue
				}
				newnames = append(newnames, v)
			}
			rdb.Set("availablenames", strings.Join(newnames, ","), 0)
			rdb.Set(cid.Value + ":secretname", secretname, 0)
			resjson := ssJSONresponse{Name: secretname, Status: 200}
			json, _ := json.Marshal(resjson)
			fmt.Printf("serving name %s to client %s\n", secretname, cid.Value)
			res.Header().Add("Content-Type", "application/json")
			res.Write(json)
			return

		}
		//fmt.Printf("Exists result: %#v\n",secretname)

		//if rdb.Exists
	} 
		
}

func (r *RootReqHandle) handleerror(res http.ResponseWriter, req *http.Request) {
	res.WriteHeader(404)
	fmt.Fprintf(res, "404")
}

func (a *AdminReqHandle) handleroot(res http.ResponseWriter, req *http.Request) {
	t := template.New("adminhome")
	ts, err := t.ParseFiles("templates/admin-main.html.tmpl")
	if err != nil {
		fmt.Printf("error parsing template: %s\n", err)
		os.Exit(1)
	}
	t_welcome := ts.Lookup("admin-main.html.tmpl")
	err = t_welcome.ExecuteTemplate(res, "admin-main.html.tmpl", a)
	if err != nil {
		fmt.Printf("Error rendering template: %s\n", err)
		res.WriteHeader(500)
		io.WriteString(res, "Error parsing template: " + err.Error())
	}
}


func (h *PagesAPIReqHandler) ReqHandler(res http.ResponseWriter, req *http.Request) {
	fmt.Println("serving /admin/")
	io.WriteString(res, "<p>admin area</p>")
}


/* standard routines
 */

func getLimitedRandInt(max int) int {
	rand.Seed(time.Now().UnixNano())
    return rand.Intn(max)
}

func newRedisConnection() (*redis.Client, error) {
	fmt.Printf("Connecting to redis: %s:%d\n", conf.RedisServerDNS, conf.RedisServerPort)
	redisurl := conf.RedisServerDNS + ":" + strconv.Itoa(conf.RedisServerPort)
	redisdb := redis.NewClient(&redis.Options{
	    Addr:     redisurl,
	    Password: "", // no password set
	    DB:       0,  // use default DB
	})
	_, err := redisdb.Ping().Result()
	if err != nil {
		return redisdb, err
	}

	return redisdb, nil
}

type ServiceConfig struct {
	Databasetype DBTYPE `json:"databasetype"`
	DBConnection string `json:"dbconnection"`
	RestfulEndpoint string `json:"restfulapi_endpoint"`
	RedisServerDNS string `json:"redis_server_dns"`
	RedisServerPort int `json:"redis_server_port"`
}

var (
	DBTypeDefault = DBTYPE("mongodb")
	DBConnectionDefault = "mongodb://127.0.0.1"
	RestfulEndpointDefault = "localhost:8080"
	RedisServerDNSDefault = "localhost"
	RedisServerPortDefault = 6379
)

func ExtractConfiguration(file string) (ServiceConfig, error) {
	conf := ServiceConfig{
		DBTypeDefault,
		DBConnectionDefault,
		RestfulEndpointDefault,
		RedisServerDNSDefault,
		RedisServerPortDefault,
	}

	f, err := os.Open(file)
	if err != nil {
		fmt.Println("config file not found.")
		fmt.Println("using default values")
		return conf, err
	}
	err = json.NewDecoder(f).Decode(&conf)

	// check environment vars and override accordingly
	redis_server := os.ExpandEnv("$REDIS_IPS")
	if len(redis_server) > 0 {
		fmt.Printf("Found env var REDIS_IPS, using value '%s'\n", redis_server)
		conf.RedisServerDNS = redis_server
	}
	redis_port := os.ExpandEnv("$ARIES_REDIS_PORT")
	if len(redis_port) > 0 {
		rp, _ := strconv.Atoi(redis_port)
		fmt.Printf("Found env var ARIES_REDIS_PORT, using value '%n'\n", rp)
		conf.RedisServerPort = rp
	}

	return conf, err
}
