package html_components


type Logo struct {
	imgsrc string
	caption string
	linktarget string
}
type Menu struct {
	levels int
	items map[string]map[string]string
}

// func InitHTMLComponents() {
// 	logo = Logo{imgsrc: "/img/logo", caption: "maxlynch.net", linktarget: "/"}
// 	menu.levels = 2
// 	menu.items = make(map[string]map[string]string)
// 	menu.items["home"]["usertext"] = "Home"
// 	menu.items["home"]["link"] = "/"
// }

func ReturnPage(page string) string {
	if page == "home" {
		menu := getMenu()
		logo := getLogo()
		return logo.getHTML() + menu.getHTML()
	}
	return ""
}

func getLogo() Logo {
	l := Logo{imgsrc: "/img/logo.png", caption: "maxlynch.net", linktarget: "/"}
	return l
}

func getMenu() Menu {
	var m Menu
	m.levels = 2
	m.items = make(map[string]map[string]string)
	m.items["home"] = make(map[string]string)
	m.items["home"]["usertext"] = "Home"
	m.items["home"]["link"] = "/"
	return m
}

func (l *Logo) getHTML() string {
	return "<a href=\"" + l.linktarget + "\"><img src=\"" + l.imgsrc + "\"/></a>"
}

func (m *Menu) getHTML() string {
	return "<a href=\"" + m.items["home"]["link"] + "\">" + m.items["home"]["usertext"] + "</a>"
}