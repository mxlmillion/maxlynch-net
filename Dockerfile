FROM golang:1.10 AS builder
EXPOSE 8010
WORKDIR	/go/src/maxlynch-net
COPY	. .
RUN	go get -d -v ./...
RUN	go install -v ./...

FROM golang:1.10-alpine
RUN mkdir -p /usr/local/go/{src,pkg,bin}
WORKDIR /usr/local/go/bin
COPY --from=builder /go/src/maxlynch-net /usr/local/go/src/maxlynch-net
COPY --from=builder /go/bin /usr/local/go/bin
COPY --from=builder /go/pkg /usr/local/go/pkg
ENTRYPOINT	["maxlynch-net"]
CMD		["-conf", "/usr/local/go/src/maxlynch-net/config/config.json"]
